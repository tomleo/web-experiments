var gulp = require('gulp'),
    webserver = require('gulp-webserver'),
    path = require('path');

var server = {
    host: 'localhost',
    port: 3333
}

gulp.task('webserver', function() {
    gulp.src('./experiments')
        .pipe(webserver({
            host: server.host,
            port: server.port,
            livereload: true,
            directoryListing: {
                enable: true,
                path: path.join(__dirname, 'experiments')
            }
        }));
});

gulp.task('watch', function() {
    gulp.watch('**/*.html');
    gulp.watch('**/css/**/*.css');
    gulp.watch('**/js/**/*.js');
});

gulp.task('default', ['watch', 'webserver']);


