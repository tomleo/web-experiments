function read_css_var(element, varName){
  const elementStyles = getComputedStyle(element);
  return elementStyles.getPropertyValue(`--${varName}`).trim();
}

function write_css_var(element, varName, value){
  return element.style.setProperty(`--${varName}`, value);
}

var h_elem = document.querySelector('.h');
console.log(read_css_var(h_elem));
write_css_var(h_elem, 'h-border-color', 'brown');

